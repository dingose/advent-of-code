#!/usr/bin/env python3

"""Advent of Code"""

import sys
import os.path


def intersecting(a, b):
    result = (
        ( (a == '-' or a == '+') and (b == '|' or b == '+') )
        or
        ( (a == '|' or a == '+') and (b == '-' or b == '+') )
    )
    # print("intersecting({}, {}) -> {}".format(a, b, result))
    return result


def go(pos, direction):
    x, y = pos
    if direction == 'U':
        y += 1
    elif direction == 'D':
        y -= 1
    elif direction == 'R':
        x += 1
    else:
        x -= 1
    return (x, y)


def main(data):
    """Main algorithm"""
    origo = (0, 0)

    # for each "cell" we need to document 1. how many steps it took to get there, and 2. in what direction we are moving

    pos = origo
    w1 = data[0]
    w1grid = dict()
    w1Loc = set()
    w1totSteps = 0

    for move in w1.split(','):
        d, steps = move[0], int(move[1:])
        for step in range(steps):
            w1totSteps += 1
            newPos = go(pos, d)
            w1Loc.add(newPos)
            direction = '-' if d in ('R', 'L') else '|'
            if steps - step == 1:
                direction = '+'
            record = (w1totSteps, direction)
            if not newPos in w1grid:
                w1grid[newPos] = record
            pos = newPos

    pos = origo
    w2 = data[1]
    w2grid = dict()
    w2Loc = set()
    w2totSteps = 0

    for move in w2.split(','):
        d, steps = move[0], int(move[1:])
        for step in range(steps):
            w2totSteps += 1
            newPos = go(pos, d)
            w2Loc.add(newPos)
            direction = '-' if d in ('R', 'L') else '|'
            if steps - step == 1:
                direction = '+'
            record = (w2totSteps, direction)
            if not newPos in w2grid:
                w2grid[newPos] = record
            pos = newPos

    # TODO: this won't cut it... an intersection is only an intersection if the wires cross, not if they are running parallel
    potentialIntersections = list(w1Loc.intersection(w2Loc))

    intersections = list()
    for pi in potentialIntersections:
        if intersecting(w1grid[pi][1], w2grid[pi][1]):
            intersections.append(pi)

    distances = [w1grid[intersection][0] + w2grid[intersection][0] for intersection in intersections]
    return min(distances)


if __name__ == '__main__':
    # TODO: I don't like this. The puzzle example specifies that it should take 610 steps to find the closest
    # path, but my algo does it in 124...
    # The other examples checks out, but I am doing something wrong here...
    # It does yield the correct answer for 3b, so I have passed it now... but still...
    testVectors = {
        #610: ['U98,R91,D20,R16,D67,R40,U7,R15,U6,R7', 'U62,R66,U55,R34,D71,R55,D58,R83'],
        410: ['R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51', 'U98,R91,D20,R16,D67,R40,U7,R15,U6,R7'],
        14: ['R5,U2,L5', 'U5,R2,D3'],
        30: ['R8,U5,L5,D3', 'U7,R6,D4,L4']
    }

    testResults = [main(ti) == eo for (eo, ti) in testVectors.items()]
    if all(testResults):
        print("All tests passed!")
        inputFile = os.path.join(
            os.path.dirname(
                os.path.abspath(sys.argv[0])
            ), 'input'
        )
        if os.path.isfile(inputFile):
            with open(inputFile, 'r') as fh:
                inputData = [line.strip() for line in fh.readlines()]
            print(main(inputData))
        else:
            print("Input file not found")
    else:
        print(testResults)

# vim: set filetype=python set foldmethod=marker
