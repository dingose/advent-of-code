#!/usr/bin/env python3

"""Advent of Code"""

import sys
import os.path


def minmax(a, b):
    return (min(a, b), max(a, b))


def manhattan(p1, p2):
    x1,y1 = p1
    x2,y2 = p2
    minX, maxX = minmax(x1, x2)
    minY, maxY = minmax(y1, y2)
    return (maxX - minX) + (maxY - minY)


def go(pos, direction):
    x, y = pos
    if direction == 'U':
        y += 1
    elif direction == 'D':
        y -= 1
    elif direction == 'R':
        x += 1
    else:
        x -= 1
    return (x, y)


def main(data):
    """Main algorithm"""
    grid = dict()
    grid[(0, 0)] = 'o'

    w1 = data[0]
    w2 = data[1]

    pos = (0, 0)
    w1Locations = set()

    for movement in w1.split(','):
        d, steps = movement[0], int(movement[1:])

        for _ in range(steps):
            pos = go(pos, d)
            w1Locations.add(pos)
            grid[pos] = '-' if d in ['L', 'R'] else '|'

        grid[pos] = '+'

    pos = (0, 0)
    w2Locations = set()

    for movement in w2.split(','):
        d, steps = movement[0], int(movement[1:])

        for _ in range(steps):
            pos = go(pos, d)
            w2Locations.add(pos)
            if pos in grid and pos in w1Locations:
                grid[pos] = 'X'
            else:
                grid[pos] = '-' if d in ['L', 'R'] else '|'

        if grid[pos] != 'X':
            grid[pos] = '+'

    intersections = list(w1Locations.intersection(w2Locations))
    origo = (0, 0)
    coord_map = dict()
    for intersection in intersections:
        coord_map[intersection] = manhattan(origo, intersection)

    # yes, technically we could just have taken min(coord_map.values())
    closest_coord = min(coord_map.items(), key=lambda x: x[1])
    return closest_coord[1]


if __name__ == '__main__':
    testVectors = {
        159: ["R75,D30,R83,U83,L12,D49,R71,U7,L72", "U62,R66,U55,R34,D71,R55,D58,R83"],
        135: ["R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51", "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7"],
    }

    testResults = [main(ti) == eo for (eo, ti) in testVectors.items()]
    if all(testResults):
        print("All tests passed!")
        inputFile = os.path.join(
            os.path.dirname(
                os.path.abspath(sys.argv[0])
            ), 'input'
        )
        if os.path.isfile(inputFile):
            with open(inputFile, 'r') as fh:
                inputData = [line.strip() for line in fh.readlines()]
            print(main(inputData))
        else:
            print("Input file not found")
    else:
        print(testResults)

# vim: set filetype=python set foldmethod=marker
