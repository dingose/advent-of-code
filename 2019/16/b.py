#!/usr/bin/env python3

"""Advent of Code"""

import os.path
import sys
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(sys.argv[0])))))
import toolbox as tb

from collections import defaultdict, deque
from itertools import cycle
from pprint import pprint

# tb.ints(str)
# tb.minmax(1, 2)
# tb.manhattan((x1,y1), (x2,y2))


def patternExtender(pattern, index):
    result = list()
    for number in pattern:
        tmp = [number] * index
        result.extend(tmp)
    return result


def runner(numbers, phases):
    base_pattern = [0, 1, 0, -1]

    for phase in range(phases):
        phasedNumbers = list()
        for idx, num in enumerate(numbers, start=1):
            pattern = cycle(patternExtender(base_pattern, idx))
            _ = next(pattern)

            multipliedNumbers = [n * p for n, p in zip(numbers, pattern)]
            summed = sum(multipliedNumbers)
            oneDigit = int(str(summed)[-1])
            phasedNumbers.append(oneDigit)

        numbers = phasedNumbers

    return ''.join([str(i) for i in numbers])


def formatter(numbers, phases):
    result = runner(numbers, phases)
    offset = int(result[0:7])
    return result[offset:offset+8]


def main(number, phases):
    """Main algorithm"""
    numbers = [int(i) for i in number[0]]
    return formatter(numbers, phases)


if __name__ == '__main__':
    testVectors = [
        {
            'expectedOutcome': '84462026',
            'testInput': {
                'number': ['80871224585914546619083218645595' * 10000],
                'phases': 100,
            },
            'function': main,
        },
        {
            'expectedOutcome': '78725270',
            'testInput': {
                'number': ['19617804207202209144916044189917' * 10000],
                'phases': 100,
            },
            'function': main,
        },
        {
            'expectedOutcome': '53553731',
            'testInput': {
                'number': ['69317163492948606335995924319873' * 10000],
                'phases': 100,
            },
            'function': main,
        },
    ]

    testResults = [
        ({
            'index': idx,
            'state': (actualOutcome := tv['function'](**tv['testInput'])) == tv['expectedOutcome'],
            'expected': tv['expectedOutcome'],
            'received': actualOutcome,
        }, print("Test {} executed with result {}".format(idx, 'PASS' if actualOutcome == tv['expectedOutcome'] else 'FAIL')))[0]
        for idx, tv
        in enumerate(testVectors)
    ]
    if all([result['state'] for result in testResults]):
        print("All tests passed!")
        inputFile = os.path.join(
            os.path.dirname(
                os.path.abspath(sys.argv[0])
            ), 'input'
        )
        if os.path.isfile(inputFile):
            with open(inputFile, 'r') as fh:
                inputData = [line.strip() * 10000 for line in fh.readlines()]
            print(main(number=inputData, phases=100))
        else:
            print("Input file not found")
    else:
        pprint(testResults)

# vim: set filetype=python set foldmethod=marker
