#!/usr/bin/env python3

"""Advent of Code"""

import os.path
import sys
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(sys.argv[0])))))
import toolbox as tb

from collections import deque

# tb.ints(str)
# tb.minmax(1, 2)
# tb.manhattan((x1,y1), (x2,y2))


def main(program):
    """Main algorithm"""
    inputStream = deque()
    outputStream = deque()
    program = tb.ints(program)

    ic = tb.Intcode(program, inputStream, outputStream)
    ic.push_input(5)
    ic.run()
    return outputStream[-1]


if __name__ == '__main__':
    testVectors = {
        '3,9,8,9,10,9,4,9,99,-1,8': 0,
        '3,9,7,9,10,9,4,9,99,-1,8': 1,
        '3,3,1108,-1,8,3,4,3,99': 0,
        '3,3,1107,-1,8,3,4,3,99': 1,
        '3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9': 1,
        '3,3,1105,-1,9,1101,0,0,12,4,12,99,1': 1,
        '3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99': 999,
    }
    testResults = [main(ti) == eo for (ti, eo) in testVectors.items()]
    if all(testResults):
        print("All tests passed!")
        inputFile = os.path.join(
            os.path.dirname(
                os.path.abspath(sys.argv[0])
            ), 'input'
        )
        if os.path.isfile(inputFile):
            with open(inputFile, 'r') as fh:
                inputData = fh.read()
                inputData = inputData.strip()
            print(main(inputData))
        else:
            print("Input file not found")
    else:
        print(testResults)

# vim: set filetype=python set foldmethod=marker
