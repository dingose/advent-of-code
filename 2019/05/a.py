#!/usr/bin/env python3

"""Advent of Code"""

import os.path
import sys
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(sys.argv[0])))))
import toolbox as tb

from collections import deque

# tb.ints(str)
# tb.minmax(1, 2)
# tb.manhattan((x1,y1), (x2,y2))


def main(program):
    """Main algorithm"""
    inputStream = deque()
    outputStream = deque()
    program = tb.ints(program)

    ic = tb.Intcode(program, inputStream, outputStream)
    ic.push_input(1)
    ic.run()
    return outputStream[-1]


if __name__ == '__main__':
    testVectors = {
        '4,0,99': 4,
        '1004,0,99': 1004,
        '3,0,4,0,99': 1,
    }
    testResults = [main(ti) == eo for (ti, eo) in testVectors.items()]
    if all(testResults):
        print("All tests passed!")
        inputFile = os.path.join(
            os.path.dirname(
                os.path.abspath(sys.argv[0])
            ), 'input'
        )
        if os.path.isfile(inputFile):
            with open(inputFile, 'r') as fh:
                inputData = fh.read()
                inputData = inputData.strip()
            print(main(inputData))
        else:
            print("Input file not found")
    else:
        print(testResults)

# vim: set filetype=python set foldmethod=marker
