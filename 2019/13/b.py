#!/usr/bin/env python3

"""Advent of Code"""

import os.path
import sys
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(sys.argv[0])))))
import toolbox as tb

from collections import defaultdict, deque
import time

# tb.ints(str)
# tb.minmax(1, 2)
# tb.manhattan((x1,y1), (x2,y2))


def main(data):
    """Main algorithm"""
    program = tb.ints(data)
    inputStream = deque()
    outputStream = deque()
    ic = tb.Intcode(program, inputStream, outputStream)
    ic.modify_program(0, 2)

    outputs = defaultdict(lambda: 0)

    paddlePos = ballPos = None
    world = defaultdict(dict)
    tileTypes = {
        0: ' ',
        1: '#',
        2: '*',
        3: '_',
        4: 'o',
    }

    while True:
        if ic.get_last_executed_opcode() == 99:
            break
        ic.run()

        # TODO, need to draw the grid (i.e. collect all the output)
        # 3 outputs = x, y and tiletype (distance from left, distance from top, type)
        # type =
        # 0. empty
        # 1. wall
        # 2. block
        # 3. horizontal paddle
        # 4. ball
        # TODO, examine state of board to figure out what input to give next
        # paddle remain at current position == input 0
        # paddle move 1 left: -1
        # paddle move 1 right: 1


        while outputStream:
            x = outputStream.popleft()
            y = outputStream.popleft()
            data = outputStream.popleft()

            if x == -1 and y == 0:
                score = data
                print("Score: {}".format(data))

            if data == 4:
                ballPos = (x, y)
            elif data == 3:
                paddlePos = (x, y)

            if x > -1:
                world[y][x] = tileTypes[data]


        print("\n"*100)
        for dy in world.keys():
            for dx in world[dy].keys():
                print(world[dy][dx], end='')
            print("")
        print(score)
        #time.sleep(0.1)


        if paddlePos and ballPos:
            if paddlePos[0] == ballPos[0]:
                inputStream.append(0)
            elif paddlePos[0] > ballPos[0]:
                inputStream.append(-1)
            else:
                inputStream.append(1)

    return score



if __name__ == '__main__':
    testVectors = [
        # add more vectors here...
    ]

    testResults = [
        {
            'index': idx,
            'state': (actualOutcome := tv['function'](**tv['testInput'])) == tv['expectedOutcome'],
            'expected': tv['expectedOutcome'],
            'received': actualOutcome,
        }
        for idx, tv
        in enumerate(testVectors)
    ]
    if all([result['state'] for result in testResults]):
        print("All tests passed!")
        inputFile = os.path.join(
            os.path.dirname(
                os.path.abspath(sys.argv[0])
            ), 'input'
        )
        if os.path.isfile(inputFile):
            with open(inputFile, 'r') as fh:
                inputData = fh.read().strip()
            print(main(inputData))
        else:
            print("Input file not found")
    else:
        print(testResults)

# vim: set filetype=python set foldmethod=marker
