#!/usr/bin/env python3

"""Advent of Code"""

import os.path
import sys
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(sys.argv[0])))))
import toolbox as tb

from collections import defaultdict, deque

# tb.ints(str)
# tb.minmax(1, 2)
# tb.manhattan((x1,y1), (x2,y2))

def falsifier(data):
    return not data


def runner(data):
    pass


def main(data):
    """Main algorithm"""
    program = tb.ints(data)
    inputStream = deque()
    outputStream = deque()
    ic = tb.Intcode(program, inputStream, outputStream)

    outputs = defaultdict(lambda: 0)

    while True:
        if ic.get_last_executed_opcode() == 99:
            break
        ic.run()

    while outputStream:
        x = outputStream.popleft()
        y = outputStream.popleft()
        tt = outputStream.popleft()
        outputs[tt] += 1

    return outputs[2]



if __name__ == '__main__':
    testVectors = [
        # add more vectors here...
    ]

    testResults = [
        {
            'index': idx,
            'state': (actualOutcome := tv['function'](**tv['testInput'])) == tv['expectedOutcome'],
            'expected': tv['expectedOutcome'],
            'received': actualOutcome,
        }
        for idx, tv
        in enumerate(testVectors)
    ]
    if all([result['state'] for result in testResults]):
        print("All tests passed!")
        inputFile = os.path.join(
            os.path.dirname(
                os.path.abspath(sys.argv[0])
            ), 'input'
        )
        if os.path.isfile(inputFile):
            with open(inputFile, 'r') as fh:
                inputData = fh.read().strip()
            print(main(inputData))
        else:
            print("Input file not found")
    else:
        print(testResults)

# vim: set filetype=python set foldmethod=marker
