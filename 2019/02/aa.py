#!/usr/bin/env python3

"""Advent of Code"""

import os.path
import sys
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(sys.argv[0])))))
import toolbox as tb

# tb.ints(str)
# tb.minmax(1, 2)
# tb.manhattan((x1,y1), (x2,y2))


def main(data, testRun=True):
    """Main algorithm"""
    ic = tb.Intcode(data)
    if not testRun:
        ic.modifyState(1, 12)
        ic.modifyState(2, 2)
    return ic.run()


if __name__ == '__main__':
    testVectors = {
        '1,9,10,3,2,3,11,0,99,30,40,50': 3500,
        '1,0,0,0,99': 2,
        '2,3,0,3,99': 2,
        '2,4,4,5,99,0': 2,
        '1,1,1,4,99,5,6,0,99': 30,
    }

    testResults = [main(ti) == eo for (ti, eo) in testVectors.items()]
    if all(testResults):
        print("All tests passed!")
        inputFile = os.path.join(
            os.path.dirname(
                os.path.abspath(sys.argv[0])
            ), 'input'
        )
        if os.path.isfile(inputFile):
            with open(inputFile, 'r') as fh:
                inputData = fh.read()
                inputData = inputData.strip()
            print(main(inputData, False))
        else:
            print("Input file not found")
    else:
        print(testResults)

# vim: set filetype=python set foldmethod=marker
