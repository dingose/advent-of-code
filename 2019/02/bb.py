#!/usr/bin/env python3

"""Advent of Code"""

"""
Ok, it took me a while to parse just what the fuck he was asking about here...
But the gist of it is:
    1. read input into state
    2. manipulate p1 and p2 (addresses 1 and 2) such that the program, once run produces 19690720 at state[0]
    3. return 100 * p1 + p2 as the answer
"""

import os.path
import sys
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(sys.argv[0])))))
import toolbox as tb


def add(a, b):
    return a + b


def multiply(a, b):
    return a * b


def runProgram(state):
    cursor = 0

    ops = {
        1: add,
        2: multiply,
    }

    while state[cursor] != 99:
        op = state[cursor]
        p1 = state[cursor + 1]
        p2 = state[cursor + 2]
        target = state[cursor + 3]

        state[target] = ops[op](state[p1], state[p2])
        cursor += 4
    return state[0]


def main(data, testRun=True):
    """Main algorithm"""
    nouns = range(0, 100)
    verbs = range(0, 100)

    for noun in nouns:
        for verb in verbs:
            ic = tb.Intcode(data)
            if not testRun:
                ic.modifyState(1, noun)
                ic.modifyState(2, verb)
            result = ic.run()
            if result == 19690720:
                return 100 * noun + verb
    return None


if __name__ == '__main__':
    inputFile = os.path.join(
        os.path.dirname(
            os.path.abspath(sys.argv[0])
        ), 'input'
    )
    if os.path.isfile(inputFile):
        with open(inputFile, 'r') as fh:
            inputData = fh.read()
            inputData = inputData.strip()
        print(main(inputData, False))
    else:
        print("Input file not found")

# vim: set filetype=python set foldmethod=marker
