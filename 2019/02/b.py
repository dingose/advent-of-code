#!/usr/bin/env python3

"""Advent of Code"""

import os.path
import sys
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(sys.argv[0])))))
import toolbox as tb

# tb.ints(str)
# tb.minmax(1, 2)
# tb.manhattan((x1,y1), (x2,y2))


def main(program):
    """Main algorithm"""
    program = tb.ints(program)

    nouns = range(100)
    verbs = range(100)

    for noun in nouns:
        for verb in verbs:
            ic = tb.Intcode(program[:])
            ic.modify_program(1, noun)
            ic.modify_program(2, verb)
            ic.run()
            if ic.dump_program_state()[0] == 19690720:
                return 100 * noun + verb

    return None


if __name__ == '__main__':
    inputFile = os.path.join(
        os.path.dirname(
            os.path.abspath(sys.argv[0])
        ), 'input'
    )
    if os.path.isfile(inputFile):
        with open(inputFile, 'r') as fh:
            inputData = fh.read()
            inputData = inputData.strip()
        print(main(inputData))
    else:
        print("Input file not found")

# vim: set filetype=python set foldmethod=marker
