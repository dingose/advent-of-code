#!/usr/bin/env python3

"""Advent of Code"""

import sys
import os.path


def main(mass):
    """Main algorithm"""
    return int(mass / 3) - 2


if __name__ == '__main__':
    testVectors = {
        12: 2,
        14: 2,
        1969: 654,
        100756: 33583
    }

    testResults = [main(mass) == fuel for (mass, fuel) in testVectors.items()]
    if all(testResults):
        print("All tests passed!")
        inputFile = os.path.join(
            os.path.dirname(
                os.path.abspath(sys.argv[0])
            ), 'input'
        )
        if os.path.isfile(inputFile):
            fuel_list = list()
            with open(inputFile, 'r') as fh:
                for line in fh:
                    line = line.strip('\n')
                    line = int(line)
                    fuel_list.append(main(line))
            print(sum(fuel_list))
        else:
            print("Input file not found")
    else:
        print(testResults)

# vim: set filetype=python set foldmethod=marker
