#!/usr/bin/env python3

"""Advent of Code"""

import sys
import os.path


def main(mass):
    """Main algorithm"""
    fuel_list = list()
    fuel_list.append(int(mass / 3) - 2)
    while True:
        mass = fuel_list[-1]
        if (fuel := int(mass/3) - 2) > 0:
            fuel_list.append(fuel)
        else:
            break
    return sum(fuel_list)




if __name__ == '__main__':
    testVectors = {
        14: 2,
        1969: 966,
        100756: 50346
    }

    testResults = [main(mass) == fuel for (mass, fuel) in testVectors.items()]
    if all(testResults):
        print("All tests passed!")
        inputFile = os.path.join(
            os.path.dirname(
                os.path.abspath(sys.argv[0])
            ), 'input'
        )
        fuel_list = list()
        if os.path.isfile(inputFile):
            with open(inputFile, 'r') as fh:
                for line in fh:
                    line = line.strip()
                    line = int(line)
                    fuel_list.append(main(line))
            print(sum(fuel_list))
        else:
            print("Input file not found")
    else:
        print(testResults)

# vim: set filetype=python set foldmethod=marker
