#!/usr/bin/env python3

"""Advent of Code"""

import os.path
import sys
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(sys.argv[0])))))
import toolbox as tb
from collections import defaultdict, deque
import random

# tb.ints(str)
# tb.minmax(1, 2)
# tb.manhattan((x1,y1), (x2,y2))

def falsifier(data):
    return not data


def allWallsFound(world):
    unknownNeighbours = list()
    knownPos = list(world.keys())

    for coord in knownPos:
        unknownNeighbours.extend(
            enumerateUnknownDirections(coord, world)
        )

    return len(unknownNeighbours) == 0


def enumerateDirections(pos, world, sign):
    """return [tuple(direction, newCoord)),..]"""
    x, y = pos

    if not pos in world:
        world[pos] = '?'
        return [pos]

    positions = {
        'S': (x, y-1),
        'N': (x, y+1),
        'W': (x-1, y),
        'E': (x+1, y)
    }
    return [
        (k, v)
        for k, v
        in positions.items()
        if world[pos] != '?' and world[v] == sign
    ]

def generatePath(pos, destination, world):
    """must return a list of movements in format tuple('N', tuple(newX, newY))"""
    curPos = pos
    path = list()
    while not curPos == destination:
        pass
    # how about networkx? ;D


def enumerateUnknownDirections(pos, world):
    return enumerateDirections(pos, world, '?')


def enumerateKnownDirections(pos, world):
    return enumerateDirections(pos, world, '.')


def main(data):
    """Main algorithm"""
    program = tb.ints(data[0])

    directions = {
        'N': 1,
        'S': 2,
        'W': 3,
        'E': 4,
    }
    possibleOutputs = {
        0: '#',
        1: '.',
        2: 'O',
    }

    inputStream = deque()
    outputStream = deque()
    ic = tb.Intcode(program, inputStream, outputStream)

    world = defaultdict(lambda: '?')
    pos = (0, 0)
    world[pos] = 'D'

    oldLen = len(world)
    pathToUnknown = deque()
    while not allWallsFound(world):
        if len(pathToUnknown) > 0:
            # follow path

            selectedDirection = None
        else
            dirsFromPos = enumerateUnknownDirections(pos, world)
            if not dirsFromPos:
                # backtrack
                # TODO: we should make this smarter, let's figure out ALL
                # unknown positions, and attempt to get there
                allUnknowns = list()
                coords = world.keys()
                for coord in coords:
                    if world[coord] == '?':
                        allUnknowns.append(coord)
                # pick a coord at random
                destination = random.choice(allUnknowns)
                pathToUnknown = generatePath(pos, destination, world)
                continue

            # pick one
            random.shuffle(dirsFromPos)
            selectedDirection = dirsFromPos[0]

        # append picked direction to inputStream
        inputStream.append( directions[selectedDirection[0]] )

        while len(outputStream) < 1:
            ic.run()

        status = outputStream.popleft()

        if status == 0:
            world[selectedDirection[1]] = '#'
        elif status == 1:
            oldPos = pos
            pos = selectedDirection[1]
            world[oldPos] = '.'
            world[pos] = 'D'
        elif status == 2:
            oldPos = pos
            pos = selectedDirection[1]
            world[oldPos] = '.'
            world[pos] = 'O'

        if oldLen != len(world) and len(world) % 10 == 0:
            xlist = list()
            ylist = list()
            for (x, y) in world.keys():
                xlist.append(x)
                ylist.append(y)

            minx, maxx = min(xlist), max(xlist)
            miny, maxy = min(ylist), max(ylist)

            for dy in range(miny, maxy+1):
                for dx in range(minx, maxx+1):
                    coord = (dx, dy)
                    print(' ' if not coord in world else world[coord], end='')
                print("")
            oldLen = len(world)



if __name__ == '__main__':
    testVectors = [
        {
            'expectedOutcome': False,
            'testInput': {
                'data': True
            },
            'function': falsifier,
        },
    ]

    testResults = [
        {
            'index': idx,
            'state': (actualOutcome := tv['function'](**tv['testInput'])) == tv['expectedOutcome'],
            'expected': tv['expectedOutcome'],
            'received': actualOutcome,
        }
        for idx, tv
        in enumerate(testVectors)
    ]
    if all([result['state'] for result in testResults]):
        print("All tests passed!")
        inputFile = os.path.join(
            os.path.dirname(
                os.path.abspath(sys.argv[0])
            ), 'input'
        )
        if os.path.isfile(inputFile):
            with open(inputFile, 'r') as fh:
                inputData = [line.strip() for line in fh.readlines()]
            print(main(inputData))
        else:
            print("Input file not found")
    else:
        print(testResults)

# vim: set filetype=python set foldmethod=marker
