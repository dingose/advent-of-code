#!/usr/bin/env python3.8

import random
import networkx as nx

world = nx.Graph()

for dy in range(0, 6):
    for dx in range(0, 6):
        coord = (dx, dy)
        if not coord in world:
            world.add_node(coord, value=".")

            adjacents = [ (dx-1, dy), (dx+1, dy), (dx, dy-1), (dx, dy+1) ]
            for adj in adjacents:
                if adj in world:
                    world.add_edge(coord, adj)

coord = random.choice(list(world.nodes))
world.nodes[coord]['value'] = 'O'

coord = random.choice( [node for node in world.nodes if not node == coord] )
world.nodes[coord]['value'] = 'D'

droidNode = [x for x, y in world.nodes(data=True) if y['value'] == 'D'][-1]
oxygenatorNode = [x for x, y in world.nodes(data=True) if y['value'] == 'O'][-1]

print("droid: ", droidNode)
print("oxygen: ", oxygenatorNode)
print(nx.shortest_path(world, droidNode, oxygenatorNode))
print(nx.shortest_path_length(world, droidNode, oxygenatorNode))
