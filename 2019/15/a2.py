#!/usr/bin/env python3

"""Advent of Code"""

import os.path
import sys
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(sys.argv[0])))))
import toolbox as tb

import networkx as nx
from collections import deque

# tb.ints(str)
# tb.minmax(1, 2)
# tb.manhattan((x1,y1), (x2,y2))

def falsifier(data):
    return not data


def updateWorld(curPos, world):
    x, y = curPos

    world.add_node( curPos, value="D" )

    adjacents = [ (x-1, y), (x+1, y), (x, y-1), (x, y+1) ]

    for neighbour in adjacents:
        if not neighbour in world:
            world.add_node( neighbour, value='?' )
            world.add_edge( curPos, neighbour )

    return world


def main(data):
    """Main algorithm"""
    program = tb.ints(data[0])
    inputStream = deque()
    outputStream = deque()
    ic = tb.Intcode(program, inputStream, outputStream)

    curPos = (0, 0)
    world = nx.Graph()

    world = updateWorld(curPos, world)

    for coord in world.nodes:
        print(world.nodes[coord])


if __name__ == '__main__':
    testVectors = [
        {
            'expectedOutcome': False,
            'testInput': {
                'data': True
            },
            'function': falsifier,
        },
        # add more vectors here...
    ]

    testResults = [
        {
            'index': idx,
            'state': (actualOutcome := tv['function'](**tv['testInput'])) == tv['expectedOutcome'],
            'expected': tv['expectedOutcome'],
            'received': actualOutcome,
        }
        for idx, tv
        in enumerate(testVectors)
    ]
    if all([result['state'] for result in testResults]):
        print("All tests passed!")
        inputFile = os.path.join(
            os.path.dirname(
                os.path.abspath(sys.argv[0])
            ), 'input'
        )
        if os.path.isfile(inputFile):
            with open(inputFile, 'r') as fh:
                inputData = [line.strip() for line in fh.readlines()]
            print(main(inputData))
        else:
            print("Input file not found")
    else:
        print(testResults)

# vim: set filetype=python set foldmethod=marker
