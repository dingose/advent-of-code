#!/usr/bin/env python3

"""Advent of Code"""

import os.path
import sys
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(sys.argv[0])))))
import toolbox as tb

import re

# tb.ints(str)
# tb.minmax(1, 2)
# tb.manhattan((x1,y1), (x2,y2))

pattern = re.compile(r'(\d)\1')
pattern2 = re.compile(r'((\d)\2+)')
notAllowedFor = { str(i): [str(c) for c in range(int(i))] for i in range(10) }


def hasDoubledNumber(data):
    if (matches := pattern2.findall(data)):
        for match in matches:
            if len(match[0]) == 2:
                return True
    return False



def hasDuplicatedNumber(data):
    if pattern.search(data):
        return True
    return False


def hasDecrease(data):
    for idx in range(len(data) - 1):
        n1 = data[idx]
        n2 = data[idx+1]
        if n2 in notAllowedFor[n1]:
            return True
    return False


def main(data):
    """Main algorithm"""
    return hasDuplicatedNumber(data) and not hasDecrease(data) and hasDoubledNumber(data)


if __name__ == '__main__':
    testVectors = {
        '112233': True,
        '123444': False,
        '111122': True,
    }

    testResults = [main(ti) == eo for (ti, eo) in testVectors.items()]
    if all(testResults):
        print("All tests passed!")
        count = 0
        for num in range(165432, 707913):
            if main(str(num)):
                count += 1
        print(count)
    else:
        print(testResults)

# vim: set filetype=python set foldmethod=marker
