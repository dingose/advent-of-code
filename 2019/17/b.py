#!/usr/bin/env python3

"""Advent of Code"""

import os.path
import sys
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(sys.argv[0])))))
import toolbox as tb

from collections import defaultdict, deque

# tb.ints(str)
# tb.minmax(1, 2)
# tb.manhattan((x1,y1), (x2,y2))

def falsifier(data):
    return not data


def main(data):
    """Main algorithm"""
    program = tb.ints(data[0])
    inputStream = deque()
    outputStream = deque()
    ic = tb.Intcode(program, inputStream, outputStream)
    ic.modify_program(0, 2)

    """
    World:
    R,10, L,12,
    R,6,
    R,10, L,12,
    R,6,
    R,6, R,10, R,12, R,6, R,10, L,12, L,12,
    R,6, R,10, R,12, R,6, R,10, L,12, L,12,
    R,6, R,10, R,12, R,6, R,10, L,12, L,12,
    R,6, R,10, R,12, R,6, R,10, L,12, R,6,
    ---
    A = R,10,L,12,R,6,
    B = R,6,R,10,R,12,R,6,
    C = R,10,L,12,L,12,


    World:
    A A B C B C B C B A
    """

    mainSeq = [ ord(c) for c in "A,A,B,C,B,C,B,C,B,A\n" ]
    A = [ ord(c) for c in "R,10,L,12,R,6\n" ]
    B = [ ord(c) for c in "R,6,R,10,R,12,R,6\n" ]
    C = [ ord(c) for c in "R,10,L,12,L,12\n" ]
    no = [ ord('n'), ord('\n')]

    for instruction in mainSeq:
        ic.push_input(instruction)

    for instruction in A:
        ic.push_input(instruction)
    for instruction in B:
        ic.push_input(instruction)
    for instruction in C:
        ic.push_input(instruction)

    for instruction in no:
        ic.push_input(instruction)

    ic.run()

    return outputStream


if __name__ == '__main__':
    testVectors = [
        {
            'expectedOutcome': False,
            'testInput': {
                'data': True
            },
            'function': falsifier,
        },
    ]

    testResults = [
        {
            'index': idx,
            'state': (actualOutcome := tv['function'](**tv['testInput'])) == tv['expectedOutcome'],
            'expected': tv['expectedOutcome'],
            'received': actualOutcome,
        }
        for idx, tv
        in enumerate(testVectors)
    ]
    if all([result['state'] for result in testResults]):
        print("All tests passed!")
        inputFile = os.path.join(
            os.path.dirname(
                os.path.abspath(sys.argv[0])
            ), 'input'
        )
        if os.path.isfile(inputFile):
            with open(inputFile, 'r') as fh:
                inputData = [line.strip() for line in fh.readlines()]
            print(main(inputData))
        else:
            print("Input file not found")
    else:
        print(testResults)

# vim: set filetype=python set foldmethod=marker
