#!/usr/bin/env python3

"""Advent of Code"""

import os.path
import sys
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(sys.argv[0])))))
import toolbox as tb

from collections import defaultdict, deque

# tb.ints(str)
# tb.minmax(1, 2)
# tb.manhattan((x1,y1), (x2,y2))

def falsifier(data):
    return not data


def isIntersection(x, y, w):
    above = (x, y-1)
    below = (x, y+1)
    left  = (x-1, y)
    right = (x+1, y)
    neighbours = [above, below, right, left]
    return all( [ w[y][x] == '#' for x, y in neighbours ] )


def main(data):
    """Main algorithm"""
    program = tb.ints(data[0])
    inputStream = deque()
    outputStream = deque()
    ic = tb.Intcode(program, inputStream, outputStream)
    ic.run()

    world = defaultdict(list)
    y = 0

    while outputStream:
        code = outputStream.popleft()
        if code == 10:
            y += 1
            continue
        world[y].append(chr(code))

    intersections = list()

    for dy in range(1, len(world) - 1):
        for dx in range(1, len(world[0]) - 1):
            if isIntersection(dx, dy, world):
                intersections.append( (dx, dy) )

    for row in world.keys():
        print(world[row])

    return sum( [x * y for x, y in intersections] )



if __name__ == '__main__':
    testVectors = [
        {
            'expectedOutcome': False,
            'testInput': {
                'data': True
            },
            'function': falsifier,
        },
    ]

    testResults = [
        {
            'index': idx,
            'state': (actualOutcome := tv['function'](**tv['testInput'])) == tv['expectedOutcome'],
            'expected': tv['expectedOutcome'],
            'received': actualOutcome,
        }
        for idx, tv
        in enumerate(testVectors)
    ]
    if all([result['state'] for result in testResults]):
        print("All tests passed!")
        inputFile = os.path.join(
            os.path.dirname(
                os.path.abspath(sys.argv[0])
            ), 'input'
        )
        if os.path.isfile(inputFile):
            with open(inputFile, 'r') as fh:
                inputData = [line.strip() for line in fh.readlines()]
            print(main(inputData))
        else:
            print("Input file not found")
    else:
        print(testResults)

# vim: set filetype=python set foldmethod=marker
