#!/usr/bin/env python3

"""Advent of Code"""

import os.path
import sys
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(sys.argv[0])))))
import toolbox as tb

# tb.ints(str)
# tb.minmax(1, 2)
# tb.manhattan((x1,y1), (x2,y2))


def runner(program, signals=[]):
    program = tb.ints(program)

    ic = tb.Intcode(program)

    for signal in signals:
        ic.push_input(signal)

    ic.run()
    return ic.peek_latest_output()


def main(program):
    """Main algorithm"""
    return runner(program, [2])


if __name__ == '__main__':
    testVectors = {
        '104,1125899906842624,99': 1125899906842624,
    }

    testResults = [runner(ti) == eo for (ti, eo) in testVectors.items()]
    if all(testResults):
        print("All tests passed!")
        inputFile = os.path.join(
            os.path.dirname(
                os.path.abspath(sys.argv[0])
            ), 'input'
        )
        if os.path.isfile(inputFile):
            with open(inputFile, 'r') as fh:
                program = fh.read()
            program = program.strip()
            print(main(program))
        else:
            print("Input file not found")
    else:
        print(testResults)

# vim: set filetype=python set foldmethod=marker
