#!/usr/bin/env python3

"""Advent of Code"""

import os.path
import sys
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(sys.argv[0])))))
import toolbox as tb

# tb.ints(str)
# tb.minmax(1, 2)
# tb.manhattan((x1,y1), (x2,y2))


def parseLine(line):
    keys = ['x', 'y', 'z']
    values = tb.ints(line)
    return { k: v for k,v in zip(keys, values) }
    


def main(steps, initialState):
    """Main algorithm"""
    moons = [
        tb.Moon(**parseLine(line))
        for line
        in initialState
    ]
    print("--- iteration 0 ---")
    for moon in moons:
        print(moon)

    for i in range(1, steps+1):
        print("--- iteration {} ---".format(i))
        for idx, moon in enumerate(moons):
            otherMoons = [otherMoon for otherMoon in moons if otherMoon != moon]
            moon.calculateVelocity(otherMoons)
        for idx, moon in enumerate(moons):
            moon.applyVelocity()
    return sum([moon.total() for moon in moons])


if __name__ == '__main__':
    testVectors = [
        {
            'expectedOutcome': 179,
            'testInput': {
                'steps': 10,
                'initialState': [ '<x=-1, y=0, z=2>', '<x=2, y=-10, z=-7>', '<x=4, y=-8, z=8>', '<x=3, y=5, z=-1>', ]
            },
            'function': main,
        },
        {
            'expectedOutcome': 1940,
            'testInput': {
                'steps': 100,
                'initialState': [ '<x=-8, y=-10, z=0>', '<x=5, y=5, z=10>', '<x=2, y=-7, z=3>', '<x=9, y=-8, z=-3>', ]
            },
            'function': main,
        },
    ]

    testResults = [
        (
            idx,
            (actualOutcome := tv['function'](**tv['testInput'])) == tv['expectedOutcome'],
            tv['expectedOutcome'],
            actualOutcome
        )
        for idx, tv
        in enumerate(testVectors)
    ]
    if all(testResults[1]):
        print("All tests passed!")
        inputFile = os.path.join(
            os.path.dirname(
                os.path.abspath(sys.argv[0])
            ), 'input'
        )
        if os.path.isfile(inputFile):
            with open(inputFile, 'r') as fh:
                inputData = [line.strip() for line in fh.readlines()]
            print(main(1000, inputData))
        else:
            print("Input file not found")
    else:
        print(testResults)

# vim: set filetype=python set foldmethod=marker
