#!/usr/bin/env python3

"""Advent of Code"""

import os.path
import sys
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(sys.argv[0])))))
import toolbox as tb

from collections import defaultdict
# tb.ints(str)
# tb.minmax(1, 2)
# tb.manhattan((x1,y1), (x2,y2))


def parseLine(line):
    keys = ['x', 'y', 'z']
    values = tb.ints(line)
    return { k: v for k,v in zip(keys, values) }


def main(initialState):
    """Main algorithm"""
    moons = [
        tb.Moon(**parseLine(line), num=idx+1)
        for idx, line
        in enumerate(initialState)
    ]

    steps = 1
    cycles = dict()

    while True:
        for idx, moon in enumerate(moons):
            otherMoons = [otherMoon for otherMoon in moons if otherMoon != moon]
            moon.calculateVelocity(otherMoons)
        for idx, moon in enumerate(moons):
            moon.applyVelocity()

        steps += 1
        for axis in "xyz":
            if all([moon.atInitialConfigurationOnAxis(axis) for moon in moons]) and not axis in cycles:
                print("found cycle on axis {}".format(axis))
                cycles[axis] = steps
        if len(cycles) == 3:
            break

    print(cycles)

    lcmXY = tb.lcm(cycles['x'], cycles['y'])
    lcmXZ = tb.lcm(cycles['x'], cycles['z'])
    lcmYZ = tb.lcm(cycles['y'], cycles['z'])

    lcmXYXZ = tb.lcm(lcmXY, lcmXZ)
    lcmXYYZ = tb.lcm(lcmXY, lcmYZ)
    lcmXZYZ = tb.lcm(lcmXZ, lcmYZ)

    if lcmXYXZ == lcmXYYZ == lcmXZYZ:
        print(lcmXYXZ)
        return lcmXYXZ
    else:
        return None


if __name__ == '__main__':
    testVectors = [
        {
            'expectedOutcome': 2772,
            'testInput': {
                'initialState': [ '<x=-1, y=0, z=2>', '<x=2, y=-10, z=-7>', '<x=4, y=-8, z=8>', '<x=3, y=5, z=-1>', ]
            },
            'function': main,
        },
        {
            'expectedOutcome': 4686774924,
            'testInput': {
                'initialState': [ '<x=-8, y=-10, z=0>', '<x=5, y=5, z=10>', '<x=2, y=-7, z=3>', '<x=9, y=-8, z=-3>', ]
            },
            'function': main,
        },
    ]

    testResults = [
        (
            idx,
            (actualOutcome := tv['function'](**tv['testInput'])) == tv['expectedOutcome'],
            tv['expectedOutcome'],
            actualOutcome
        )
        for idx, tv
        in enumerate(testVectors)
    ]
    if all([result[1] for result in testResults]):
        print("All tests passed!")
        inputFile = os.path.join(
            os.path.dirname(
                os.path.abspath(sys.argv[0])
            ), 'input'
        )
        if os.path.isfile(inputFile):
            with open(inputFile, 'r') as fh:
                inputData = [line.strip() for line in fh.readlines()]
            print(main(inputData))
        else:
            print("Input file not found")
    else:
        print(testResults)

# vim: set filetype=python set foldmethod=marker
