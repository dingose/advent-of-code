#!/usr/bin/env python3

"""Advent of Code"""

import os.path
import sys
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(sys.argv[0])))))
import toolbox as tb

import copy
from collections import defaultdict, deque

# tb.ints(str)
# tb.minmax(1, 2)
# tb.manhattan((x1,y1), (x2,y2))

directionMap = {
    'U': { 'L': 'L', 'R': 'R', },
    'L': { 'L': 'D', 'R': 'U', },
    'R': { 'L': 'U', 'R': 'D', },
    'D': { 'L': 'R', 'R': 'L', },
}

def falsifier(data):
    return not data


def turnAndMove(curPos, facing, turn):
    assert turn in ('L', 'R')
    assert facing in ('L', 'R', 'U', 'D')
    x, y = curPos
    newFacing = directionMap[facing][turn]
    if newFacing == 'U':
        y -= 1
    elif newFacing == 'D':
        y += 1
    elif newFacing == 'L':
        x -= 1
    else:
        x += 1

    newPos = (x, y)
    return (newPos, newFacing)


def main(data):
    """Main algorithm"""
    program = tb.ints(data)
    panelsTouched = set()
    inputStream = deque()
    outputStream = deque()
    ic = tb.Intcode(program, inputStream, outputStream)

    grid = defaultdict(lambda: 0)

    facing = 'U'
    curPos = (0, 0)
    firstRun = True
    while True:
        last_opcode = ic.get_last_executed_opcode()
        if last_opcode == 99:
            break

        panelsTouched.add(curPos)
        if not firstRun:
            inputStream.append(grid[curPos])
        else:
            inputStream.append(1)
            grid[curPos] = 1
            firstRun = False
        ic.run()
        grid[curPos] = outputStream.popleft()
        turn = 'L' if outputStream.popleft() == 0 else 'R'
        curPos, facing = turnAndMove(curPos, facing, turn)

    xmin = xmax = ymin = ymax = 0
    for (x,y) in grid.keys():
        xmin = min(xmin, x)
        xmax = max(xmax, x)
        ymin = min(ymin, y)
        ymax = max(ymax, y)

    for dy in range(ymin, ymax+1):
        for dx in range(xmin, xmax+1):
            char = "#" if grid[(dx,dy)] == 1 else " "
            print("{}".format(char), end='')
        print("")


if __name__ == '__main__':
    inputFile = os.path.join(
        os.path.dirname(
            os.path.abspath(sys.argv[0])
        ), 'input'
    )
    if os.path.isfile(inputFile):
        with open(inputFile, 'r') as fh:
            inputData = fh.read().strip()
        print(main(inputData))
    else:
        print("Input file not found")

# vim: set filetype=python set foldmethod=marker
