#!/usr/bin/env python3

"""Advent of Code"""

import os.path
import sys
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(sys.argv[0])))))
import toolbox as tb

from collections import defaultdict
from itertools import cycle
from pprint import pprint

# tb.ints(str)
# tb.minmax(1, 2)
# tb.manhattan((x1,y1), (x2,y2))


def main(data, center, steps):
    """Main algorithm"""
    asteroids = dict()
    quadrants = cycle("N,NE,E,SE,S,SW,W,NW".split(','))

    inclineOrders = defaultdict(list)
    asteroidList = list()

    for y in range(len(data)):
        for x in range(len(data[y])):
            coord = (x, y)
            if data[y][x] == '#' and coord != center:
                asteroid = tb.Asteroid(x, y, center)
                asteroidList.append(asteroid)

    # asteroids can be sorted just fine,
    # list of tuples however... hoo boy...
    asteroidList.sort()
    for asteroid in asteroidList:
        quad = asteroid.quadrant()
        slope = asteroid.incline()

        if not slope in inclineOrders[quad]:
            inclineOrders[quad].append(slope)

        if not quad in asteroids.keys():
            asteroids[quad] = defaultdict(list)

        asteroids[quad][slope].append(asteroid)

    for quad in asteroids.keys():
        for slope in inclineOrders[quad]:
            if len(asteroids[quad][slope]) > 1:
                asteroids[quad][slope].sort(key=lambda x: x.distance())

    destroyedAsteroids = list()
    while len(destroyedAsteroids) < steps:
        quad = next(quadrants)
        for slope in inclineOrders[quad]:
            try:
                destroyedAsteroid = asteroids[quad][slope].pop(0)
                destroyedAsteroids.append( destroyedAsteroid )
            except IndexError:
                pass

    asteroid = destroyedAsteroids[steps-1]
    print(asteroid)
    return asteroid.coord()[0] * 100 + asteroid.coord()[1]


if __name__ == '__main__':
    testVectors = [
        {
            'testInput': {
                'data': [ ".#..##.###...#######", "##.############..##.", ".#.######.########.#", ".###.#######.####.#.", "#####.##.#.##.###.##", "..#####..#.#########", "####################", "#.####....###.#.#.##", "##.#################", "#####.##.###..####..", "..######..##.#######", "####.##.####...##..#", ".#####..#.######.###", "##...#.##########...", "#.##########.#######", ".####.#.###.###.#.##", "....##.##.###..#####", ".#.#.###########.###", "#.#.#.#####.####.###", "###.##.####.##.#..##", ],
                'center': (11, 13),
                'steps': 1,
            },
            'function': main,
            'expectedOutcome': 1112,
        },
        {
            'testInput': {
                'data': [ ".#..##.###...#######", "##.############..##.", ".#.######.########.#", ".###.#######.####.#.", "#####.##.#.##.###.##", "..#####..#.#########", "####################", "#.####....###.#.#.##", "##.#################", "#####.##.###..####..", "..######..##.#######", "####.##.####...##..#", ".#####..#.######.###", "##...#.##########...", "#.##########.#######", ".####.#.###.###.#.##", "....##.##.###..#####", ".#.#.###########.###", "#.#.#.#####.####.###", "###.##.####.##.#..##", ],
                'center': (11, 13),
                'steps': 2,
            },
            'function': main,
            'expectedOutcome': 1201,
        },
        {
            'testInput': {
                'data': [ ".#..##.###...#######", "##.############..##.", ".#.######.########.#", ".###.#######.####.#.", "#####.##.#.##.###.##", "..#####..#.#########", "####################", "#.####....###.#.#.##", "##.#################", "#####.##.###..####..", "..######..##.#######", "####.##.####...##..#", ".#####..#.######.###", "##...#.##########...", "#.##########.#######", ".####.#.###.###.#.##", "....##.##.###..#####", ".#.#.###########.###", "#.#.#.#####.####.###", "###.##.####.##.#..##", ],
                'center': (11, 13),
                'steps': 200,
            },
            'function': main,
            'expectedOutcome': 802,
        },
        # add more vectors here...
    ]

    testResults = [
        (
            idx,
            (actualOutcome := tv['function'](**tv['testInput'])) == tv['expectedOutcome'],
            tv['expectedOutcome'],
            actualOutcome
        )
        for idx, tv
        in enumerate(testVectors)
    ]
    if all([result[1] for result in testResults]):
        print("All tests passed!")
        inputFile = os.path.join(
            os.path.dirname(
                os.path.abspath(sys.argv[0])
            ), 'input'
        )
        if os.path.isfile(inputFile):
            with open(inputFile, 'r') as fh:
                inputData = [line.strip() for line in fh.readlines()]
            print(main(data=inputData, center=(17,22), steps=200))
        else:
            print("Input file not found")
    else:
        print(testResults)
        #print([result for result in testResults if not result[1]])

# vim: set filetype=python set foldmethod=marker
