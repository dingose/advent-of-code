#!/usr/bin/env python3

"""Advent of Code"""

import os.path
import sys
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(sys.argv[0])))))
import toolbox as tb

from collections import defaultdict
from pprint import pprint

# tb.ints(str)
# tb.minmax(1, 2)
# tb.manhattan((x1,y1), (x2,y2))

asteroids = [
    (5,1), (5,2), (5,3), (6,2), (6,3), (6,5), (7,1), (7,3), (7,4)
]

sortedAsteroids = list()

c = (5,5)

for a in asteroids:
    for b in [x for x in asteroids if x != a]:
        print("{} right of {}? {}".format(a, b, tb.greaterThan(a, b, c)))

# vim: set filetype=python set foldmethod=marker
