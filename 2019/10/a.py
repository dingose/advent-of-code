#!/usr/bin/env python3

"""Advent of Code"""

import os.path
import sys
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(sys.argv[0])))))
import toolbox as tb

from collections import defaultdict
from pprint import pprint

# tb.ints(str)
# tb.minmax(1, 2)
# tb.manhattan((x1,y1), (x2,y2))


def main(data):
    """Main algorithm"""
    print("--- New Run ---")
    asteroids = defaultdict(dict)

    for y in range(len(data)):
        for x in range(len(data[y])):
            if data[y][x] == "#":
                asteroids[(x, y)] = {
                    'N': set(), 'S': set(),
                    'NW': set(), 'NE': set(),
                    'W': set(), 'E': set(),
                    'SW': set(), 'SE': set(),
                }

    for asteroid in asteroids:
        otherAsteroids = [otherAsteroid for otherAsteroid in asteroids.keys() if otherAsteroid != asteroid]
        for otherAsteroid in otherAsteroids:
            asteroids[asteroid][tb.getQuadOf(origin=asteroid, other=otherAsteroid)].add(
                tb.determineSmallestGradientBetween(asteroid, otherAsteroid)
            )

    maxSightings = 0
    bestAsteroid = None
    for asteroid, mapping in asteroids.items():
        tmpSightings = 0
        for quad in mapping.keys():
            tmpSightings += len(mapping[quad])
        if tmpSightings > maxSightings:
            maxSightings = tmpSightings
            bestAsteroid = asteroid

    return maxSightings



if __name__ == '__main__':
    testVectors = {
        8: [ ".#..#", ".....", "#####", "....#", "...##", ],
        33: [ "......#.#.", "#..#.#....", "..#######.", ".#.#.###..", ".#..#.....", "..#....#.#", "#..#....#.", ".##.#..###", "##...#..#.", ".#....####", ],
        35: [ "#.#...#.#.", ".###....#.", ".#....#...", "##.#.#.#.#", "....#.#.#.", ".##..###.#", "..#...##..", "..##....##", "......#...", ".####.###.", ],
        41: [ ".#..#..###", "####.###.#", "....###.#.", "..###.##.#", "##.##.#.#.", "....###..#", "..#.#..#.#", "#..#.#.###", ".##...##.#", ".....#.#..", ],
        210: [ ".#..##.###...#######", "##.############..##.", ".#.######.########.#", ".###.#######.####.#.", "#####.##.#.##.###.##", "..#####..#.#########", "####################", "#.####....###.#.#.##", "##.#################", "#####.##.###..####..", "..######..##.#######", "####.##.####...##..#", ".#####..#.######.###", "##...#.##########...", "#.##########.#######", ".####.#.###.###.#.##", "....##.##.###..#####", ".#.#.###########.###", "#.#.#.#####.####.###", "###.##.####.##.#..##", ],
    }

    testResults = [main(ti) == eo for (eo, ti) in testVectors.items()]
    if all(testResults):
        print("All tests passed!")
        inputFile = os.path.join(
            os.path.dirname(
                os.path.abspath(sys.argv[0])
            ), 'input'
        )
        if os.path.isfile(inputFile):
            with open(inputFile, 'r') as fh:
                inputData = [line.strip() for line in fh.readlines()]
            print(main(inputData))
        else:
            print("Input file not found")
    else:
        print(testResults)

# vim: set filetype=python set foldmethod=marker
