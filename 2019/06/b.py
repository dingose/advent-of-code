#!/usr/bin/env python3

"""Advent of Code"""

import os.path
import sys
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(sys.argv[0])))))
import toolbox as tb

from collections import defaultdict

# tb.ints(str)
# tb.minmax(1, 2)
# tb.manhattan((x1,y1), (x2,y2))


def main(data):
    """Main algorithm"""
    orbits = dict()
    for orbit in data:
        a, b = orbit.split(')')
        orbits[b] = a

    p1 = set()
    tmpBody = orbits['YOU']
    while tmpBody in orbits.keys():
        p1.add(tmpBody)
        tmpBody = orbits.get(tmpBody)

    p2 = set()
    tmpBody = orbits['SAN']
    while tmpBody in orbits.keys():
        p2.add(tmpBody)
        tmpBody = orbits.get(tmpBody)

    return len(p1.difference(p2)) + len(p2.difference(p1))


if __name__ == '__main__':
    testVectors = [
        (1, ['COM)A', 'A)YOU', 'COM)SAN',]),
        (2, ['COM)A', 'A)YOU', 'COM)B', 'B)SAN',]),
        (4, ['COM)B', 'B)C', 'C)D', 'D)E', 'E)F', 'B)G', 'G)H', 'D)I', 'E)J', 'J)K', 'K)L', 'K)YOU', 'I)SAN',]),
    ]

    testResults = [main(ti) == eo for (eo, ti) in testVectors]
    if all(testResults):
        print("All tests passed!")
        inputFile = os.path.join(
            os.path.dirname(
                os.path.abspath(sys.argv[0])
            ), 'input'
        )
        if os.path.isfile(inputFile):
            data = list()
            with open(inputFile, 'r') as fh:
                for line in fh:
                    line = line.strip()
                    data.append(line)
            print(main(data))
        else:
            print("Input file not found")
    else:
        print(testResults)

# vim: set filetype=python set foldmethod=marker
