#!/usr/bin/env python3

"""Advent of Code"""

import os.path
import sys
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(sys.argv[0])))))
import toolbox as tb

from collections import defaultdict

# tb.ints(str)
# tb.minmax(1, 2)
# tb.manhattan((x1,y1), (x2,y2))


def main(data):
    """Main algorithm"""
    orbits = dict()
    bodies = set()
    for orbit in data:
        a, b = orbit.split(')')
        bodies.add(a)
        bodies.add(b)
        orbits[b] = a

    orbitCounts = list()
    for body in bodies:
        tmpCount = 0
        tmpBody = body
        while tmpBody in orbits.keys():
            tmpCount += 1
            tmpBody = orbits.get(tmpBody)
        orbitCounts.append(tmpCount)

    return sum(orbitCounts)


if __name__ == '__main__':
    testVectors = [
        (42, ['COM)B', 'B)C', 'C)D', 'D)E', 'E)F', 'B)G', 'G)H', 'D)I', 'E)J', 'J)K', 'K)L',]),
        (1, ['COM)B',]),
        (3, ['COM)B', 'B)C']),
        (2, ['COM)B', 'COM)C']),
    ]

    testResults = [main(ti) == eo for (eo, ti) in testVectors]
    if all(testResults):
        print("All tests passed!")
        inputFile = os.path.join(
            os.path.dirname(
                os.path.abspath(sys.argv[0])
            ), 'input'
        )
        if os.path.isfile(inputFile):
            data = list()
            with open(inputFile, 'r') as fh:
                for line in fh:
                    line = line.strip()
                    data.append(line)
            print(main(data))
        else:
            print("Input file not found")
    else:
        print(testResults)

# vim: set filetype=python set foldmethod=marker
