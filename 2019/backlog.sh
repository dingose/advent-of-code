#!/bin/bash -eu

find ./ -mindepth 1 -type d | sort | while read day;
do
    if [ ! -f $day/apass ];
    then
        echo "$day/a"
        echo "$day/b"
    else
        if [ ! -f $day/bpass ];
        then
            echo "> $day/b"
        fi
    fi
done
