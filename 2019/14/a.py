#!/usr/bin/env python3

"""Advent of Code"""

import os.path
import sys
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(sys.argv[0])))))
import toolbox as tb

from collections import defaultdict, deque
from itertools import cycle
from pprint import pprint


# We need a good data structure,
# like what reagent output, and qty, we get using certain input reagents and quantities
# something along the lines of:

# mapping = {
    # 'outputReagent' : [
        # {
            # 'outputQuantity': int(),
            # 'inputReagents': {
                # 'inputReagent': inputQuantity,
                # ..., # more than one reagent might be needed to produce the outputReagent
            # }
        # },
        # ... # there may be more than one combination yielding this outputReagent
    # ]
# }

# I feel like the algorithm we need here is probably one of those planning tree-thingies, like that falling tower erlang game.

def setup(data):
    reagents = defaultdict(list)
    for line in data:
        (requirements, output) = line.split(' => ')

        (outputQuantity, outputReagent) = output.split(' ')
        outputQuantity = int(outputQuantity)

        reagentMap = dict()
        reagentMap['outputQuantity'] = outputQuantity
        reagentMap['inputReagents'] = dict()

        for requirement in requirements.split(', '):
            inputQuantity, inputReagent = requirement.split(' ')
            inputQuantity = int(inputQuantity)

            reagentMap['inputReagents'][inputReagent] = inputQuantity

        reagents[outputReagent].append(reagentMap)
    return reagents


def main(data):
    """Main algorithm"""
    oreCount = 0
    reagents = setup(data)
    pprint(reagents)
    inventory = defaultdict(lambda: 0)
    fuelTarget = 1

    shoppingList = list()
    while inventory['FUEL'] < fuelTarget:
        if len(reagents['FUEL']) > 1:
            # TODO, need to figure out a way
            # to find the minimal ...
            # actually.. this is a weighted graph traversal, ain't it...
            # of some sort, somehow...
            print("pass1")
            pass
        else:
            shoppingList.append(reagents['FUEL'])

        while shoppingList:
            recipes = shoppingList.pop(0)
            # we now have a list of potential recipes to follow
            if len(recipes) == 1:
                recipe = recipes[0]
            else:
                #TODO: figure out the minimal amount of ore each of the trees
                # will require to get the needed amount of fuel
                print("pass2")
                pass

            if 'ORE' in recipe['inputReagents']:
                oreCount += recipe['inputReagents']['ORE']
                print("hit")
            else:
                print("miss")
                shoppingList.append(reagents[reagent])

            for reagent in recipe['inputReagents']:
                inventory[reagent] += recipe['inputReagents'][reagent]



    return oreCount


if __name__ == '__main__':
    testVectors = [
        {
            'expectedOutcome': 1,
            'testInput': {
                'data': [
                    '1 ORE => 1 FUEL',
                ]
            },
            'function': main,
        },
        {
            'expectedOutcome': 31,
            'testInput': {
                'data': [
                    '10 ORE => 10 A',
                    '1 ORE => 1 B',
                    '7 A, 1 B => 1 C',
                    '7 A, 1 C => 1 D',
                    '7 A, 1 D => 1 E',
                    '7 A, 1 E => 1 FUEL',
                ]
            },
            'function': main,
        },
        {
            'expectedOutcome': 165,
            'testInput': {
                'data': [
                    '9 ORE => 2 A',
                    '8 ORE => 3 B',
                    '7 ORE => 5 C',
                    '3 A, 4 B => 1 AB',
                    '5 B, 7 C => 1 BC',
                    '4 C, 1 A => 1 CA',
                    '2 AB, 3 BC, 4 CA => 1 FUEL',
                ]
            },
            'function': main,
        },
        {
            'expectedOutcome': 13312,
            'testInput': {
                'data': [
                    '157 ORE => 5 NZVS',
                    '165 ORE => 6 DCFZ',
                    '44 XJWVT, 5 KHKGT, 1 QDVJ, 29 NZVS, 9 GPVTF, 48 HKGWZ => 1 FUEL',
                    '12 HKGWZ, 1 GPVTF, 8 PSHF => 9 QDVJ',
                    '179 ORE => 7 PSHF',
                    '177 ORE => 5 HKGWZ',
                    '7 DCFZ, 7 PSHF => 2 XJWVT',
                    '165 ORE => 2 GPVTF',
                    '3 DCFZ, 7 NZVS, 5 HKGWZ, 10 PSHF => 8 KHKGT',
                ]
            },
            'function': main,
        },
        {
            'expectedOutcome': 180697,
            'testInput': {
                'data': [
                    '2 VPVL, 7 FWMGM, 2 CXFTF, 11 MNCFX => 1 STKFG',
                    '17 NVRVD, 3 JNWZP => 8 VPVL',
                    '53 STKFG, 6 MNCFX, 46 VJHF, 81 HVMC, 68 CXFTF, 25 GNMV => 1 FUEL',
                    '22 VJHF, 37 MNCFX => 5 FWMGM',
                    '139 ORE => 4 NVRVD',
                    '144 ORE => 7 JNWZP',
                    '5 MNCFX, 7 RFSQX, 2 FWMGM, 2 VPVL, 19 CXFTF => 3 HVMC',
                    '5 VJHF, 7 MNCFX, 9 VPVL, 37 CXFTF => 6 GNMV',
                    '145 ORE => 6 MNCFX',
                    '1 NVRVD => 8 CXFTF',
                    '1 VJHF, 6 MNCFX => 4 RFSQX',
                    '176 ORE => 6 VJHF',
                ]
            },
            'function': main,
        },
        {
            'expectedOutcome': 2210736,
            'testInput': {
                'data': [
                    '171 ORE => 8 CNZTR',
                    '7 ZLQW, 3 BMBT, 9 XCVML, 26 XMNCP, 1 WPTQ, 2 MZWV, 1 RJRHP => 4 PLWSL',
                    '114 ORE => 4 BHXH',
                    '14 VRPVC => 6 BMBT',
                    '6 BHXH, 18 KTJDG, 12 WPTQ, 7 PLWSL, 31 FHTLT, 37 ZDVW => 1 FUEL',
                    '6 WPTQ, 2 BMBT, 8 ZLQW, 18 KTJDG, 1 XMNCP, 6 MZWV, 1 RJRHP => 6 FHTLT',
                    '15 XDBXC, 2 LTCX, 1 VRPVC => 6 ZLQW',
                    '13 WPTQ, 10 LTCX, 3 RJRHP, 14 XMNCP, 2 MZWV, 1 ZLQW => 1 ZDVW',
                    '5 BMBT => 4 WPTQ',
                    '189 ORE => 9 KTJDG',
                    '1 MZWV, 17 XDBXC, 3 XCVML => 2 XMNCP',
                    '12 VRPVC, 27 CNZTR => 2 XDBXC',
                    '15 KTJDG, 12 BHXH => 5 XCVML',
                    '3 BHXH, 2 VRPVC => 7 MZWV',
                    '121 ORE => 7 VRPVC',
                    '7 XCVML => 6 RJRHP',
                    '5 BHXH, 4 VRPVC => 5 LTCX',
                ]
            },
            'function': main,
        },
    ]

    testResults = [
        ({
            'index': idx,
            'state': (actualOutcome := tv['function'](**tv['testInput'])) == tv['expectedOutcome'],
            'expected': tv['expectedOutcome'],
            'received': actualOutcome,
        }, print("\aTest {} executed with result {}".format(idx, 'PASS' if actualOutcome == tv['expectedOutcome'] else 'FAIL')))[0]
        for idx, tv
        in enumerate(testVectors)
    ]
    if all([result['state'] for result in testResults]):
        print("All tests passed!")
        inputFile = os.path.join(
            os.path.dirname(
                os.path.abspath(sys.argv[0])
            ), 'input'
        )
        if os.path.isfile(inputFile):
            with open(inputFile, 'r') as fh:
                inputData = [line.strip() for line in fh.readlines()]
            print(main(inputData))
        else:
            print("Input file not found")
    else:
        pprint(testResults)

# vim: set filetype=python set foldmethod=marker
