#!/usr/bin/env python3

"""Advent of Code"""

import os.path
import sys
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(sys.argv[0])))))
import toolbox as tb

import math
from collections import defaultdict, deque

# tb.ints(str)
# tb.minmax(1, 2)
# tb.manhattan((x1,y1), (x2,y2))


def setup(program: list) -> list:
    inputStream = deque()
    outputStream = deque()

    world = list()

    for y in range(50):
        world.append(list())
        for x in range(50):
            ic = tb.Intcode(program, inputStream, outputStream)
            ic.push_input(x)
            ic.push_input(y)
            ic.run()
            world[y].append( '#' if ic.pop_output() == 1 else '.' )

    return world


def foo(world):
    for idx in reversed(range(len(world))):
        if not '#' in world[idx]:
            continue
        else:
            break
    y = idx
    for idx, char in enumerate(world[y]):
        if char == '#':
            break
    x = idx
    return (x, y)


def runner(world: list) -> tuple:
    """
    input: list of lists (2d array),
    output coordinate (tuple)
    """
    # TODO: we need to figure out the ray cast lines
    # or rather, the slopes
    # Day 10
    origin = (0, 0)

    firstBeamOnLowestRow = foo(world)
    assert firstBeamOnLowestRow == (29, 34)
    print(math.gcd(29,34))

    print( tb.determineSmallestGradientBetween(origin, firstBeamOnLowestRow) )
    #highestBeamOnXmax = bar(world)

    print("point on line: ", tb.point_on_line( (2,2), ( origin, firstBeamOnLowestRow )))



def main(data: list) -> int:
    """Main algorithm"""
    program = tb.ints(data[0])
    world = setup(program)
    (x, y) = runner(world)
    return (x * 10000) + y


if __name__ == '__main__':
    testVectors = [
        {
            'expectedOutcome': (25, 20),
            'testInput': {
                'world': [
                    list('#.......................................'),
                    list('.#......................................'),
                    list('..##....................................'),
                    list('...###..................................'),
                    list('....###.................................'),
                    list('.....####...............................'),
                    list('......#####.............................'),
                    list('......######............................'),
                    list('.......#######..........................'),
                    list('........########........................'),
                    list('.........#########......................'),
                    list('..........#########.....................'),
                    list('...........##########...................'),
                    list('...........############.................'),
                    list('............############................'),
                    list('.............#############..............'),
                    list('..............##############............'),
                    list('...............###############..........'),
                    list('................###############.........'),
                    list('................#################.......'),
                    list('.................########OOOOOOOOOO.....'),
                    list('..................#######OOOOOOOOOO#....'),
                    list('...................######OOOOOOOOOO###..'),
                    list('....................#####OOOOOOOOOO#####'),
                    list('.....................####OOOOOOOOOO#####'),
                    list('.....................####OOOOOOOOOO#####'),
                    list('......................###OOOOOOOOOO#####'),
                    list('.......................##OOOOOOOOOO#####'),
                    list('........................#OOOOOOOOOO#####'),
                    list('.........................OOOOOOOOOO#####'),
                    list('..........................##############'),
                    list('..........................##############'),
                    list('...........................#############'),
                    list('............................############'),
                    list('.............................###########'),
                ],
            },
            'function': runner,
        },
        # add more vectors here...
    ]

    testResults = [
        {
            'index': idx,
            'state': (actualOutcome := tv['function'](**tv['testInput'])) == tv['expectedOutcome'],
            'expected': tv['expectedOutcome'],
            'received': actualOutcome,
        }
        for idx, tv
        in enumerate(testVectors)
    ]
    if all([result['state'] for result in testResults]):
        print("All tests passed!")
        inputFile = os.path.join(
            os.path.dirname(
                os.path.abspath(sys.argv[0])
            ), 'input'
        )
        if os.path.isfile(inputFile):
            with open(inputFile, 'r') as fh:
                inputData = [line.strip() for line in fh.readlines()]
            print(main(inputData))
        else:
            print("Input file not found")
    else:
        print(testResults)

# vim: set filetype=python set foldmethod=marker
