#!/usr/bin/env python3

"""Advent of Code"""

import os.path
import sys
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(sys.argv[0])))))
import toolbox as tb

from collections import defaultdict, deque

# tb.ints(str)
# tb.minmax(1, 2)
# tb.manhattan((x1,y1), (x2,y2))

def falsifier(data):
    return not data


def runner(data):
    pass


def main(data):
    """Main algorithm"""
    program = tb.ints(data[0])
    inputStream = deque()
    outputStream = deque()


    world = defaultdict(dict)

    # first of all, will the program accept multiple input coordinates?
    # or will I need to launch a new IC for each coord?

    for y in range(50):
        for x in range(50):
            ic = tb.Intcode(program, inputStream, outputStream)
            ic.push_input(x)
            ic.push_input(y)
            ic.run()
            world[y][x] = ic.pop_output()

    result = 0
    for row in sorted(world.keys()):
        for col in sorted(world[row].keys()):
            print(world[row][col], end='')
            result += world[row][col]
        print("")

    return result


if __name__ == '__main__':
    testVectors = [
        {
            'expectedOutcome': False,
            'testInput': {
                'data': True
            },
            'function': falsifier,
        },
        # add more vectors here...
    ]

    testResults = [
        {
            'index': idx,
            'state': (actualOutcome := tv['function'](**tv['testInput'])) == tv['expectedOutcome'],
            'expected': tv['expectedOutcome'],
            'received': actualOutcome,
        }
        for idx, tv
        in enumerate(testVectors)
    ]
    if all([result['state'] for result in testResults]):
        print("All tests passed!")
        inputFile = os.path.join(
            os.path.dirname(
                os.path.abspath(sys.argv[0])
            ), 'input'
        )
        if os.path.isfile(inputFile):
            with open(inputFile, 'r') as fh:
                inputData = [line.strip() for line in fh.readlines()]
            print(main(inputData))
        else:
            print("Input file not found")
    else:
        print(testResults)

# vim: set filetype=python set foldmethod=marker
