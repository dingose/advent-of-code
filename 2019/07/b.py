#!/usr/bin/env python3

"""Advent of Code"""

import os.path
import sys
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(sys.argv[0])))))
import toolbox as tb

import itertools

from collections import deque

# tb.ints(str)
# tb.minmax(1, 2)
# tb.manhattan((x1,y1), (x2,y2))

NUMBER_OF_AMPS = 5
PHASE_SETTING_NUMBERS = range(5, 10)


def runner(icList):
    while True:
        output = feedbackLoop(icList)
        print(output)
        last_opcode = icList[-1].get_last_executed_opcode()
        if last_opcode == 99:
            return output


def feedbackLoop(icList):
    for tmpIC in icList:
        tmpIC.run()

    return tmpIC.peek_latest_output()


def setup(program, sequence):
    program = tb.ints(program)
    # create a new list of Intcode parsers
    # their first input signal should be from the sequence (the phase setting from
    # the problem description)
    icList = list()
    for ampId in range(NUMBER_OF_AMPS):
        ic = tb.Intcode(program[:], deque(), deque())
        ic.push_input(sequence[ampId])
        icList.append(ic)

    # hook it up so that the inputStream of IC X,
    # is also the outputStream of IC X-1
    for idx in range(len(icList)):
        icList[(idx-1)%len(icList)]._output = icList[idx]._input

    # the first time the Amp A IC should also get 0 as it's first additional
    # input signal (the other additional input signals from here on out should
    # be the output from the preceeding Amp IC)
    icList[0].push_input(0)

    return icList


def testShim(program, sequence):
    icList = setup(program, sequence)
    return runner(icList)


def main(program):
    """Main algorithm"""
    outputs = list()
    for sequence in itertools.permutations(PHASE_SETTING_NUMBERS): # tuple of values
        icList = setup(program, sequence)
        outputs.append(runner(icList))
    return max(outputs)


if __name__ == '__main__':
    testVectors = {
        139629729: ("3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5", [9,8,7,6,5]),
        18216: ("3,52,1001,52,-5,52,3,53,1,52,56,54,1007,54,5,55,1005,55,26,1001,54,-5,54,1105,1,12,1,53,54,53,1008,54,0,55,1001,55,1,55,2,53,55,53,4,53,1001,56,-1,56,1005,56,6,99,0,0,0,0,10", [9,7,8,5,6]),
    }

    testResults = [testShim(ti[0], ti[1]) == eo for (eo, ti) in testVectors.items()]
    if all(testResults):
        print("All tests passed!")
        inputFile = os.path.join(
            os.path.dirname(
                os.path.abspath(sys.argv[0])
            ), 'input'
        )
        if os.path.isfile(inputFile):
            with open(inputFile, 'r') as fh:
                program = fh.read()
            program = program.strip()
            print(main(program))
        else:
            print("Input file not found")
    else:
        print(testResults)

# vim: set filetype=python set foldmethod=marker
