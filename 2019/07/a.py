#!/usr/bin/env python3

"""Advent of Code"""

import os.path
import sys
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(sys.argv[0])))))
import toolbox as tb

import itertools

from collections import deque

# tb.ints(str)
# tb.minmax(1, 2)
# tb.manhattan((x1,y1), (x2,y2))


def runner(program, sequence):
    program = tb.ints(program)
    result = 0
    for ampId in range(5):
        inputStream = deque()
        outputStream = deque()

        ic = tb.Intcode(program[:], inputStream, outputStream)
        ic.push_input(sequence[ampId])
        ic.push_input(result)

        ic.run()
        result = ic.pop_output()

    return result


def main(program):
    """Main algorithm"""
    inputs = range(5)
    outputs = list()

    for sequence in itertools.permutations(inputs): # tuple of values
        outputs.append(runner(program, sequence))
    return max(outputs)


if __name__ == '__main__':
    testVectors = {
        43210: ("3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0", [4,3,2,1,0]),
        54321: ("3,23,3,24,1002,24,10,24,1002,23,-1,23,101,5,23,23,1,24,23,23,4,23,99,0,0", [0,1,2,3,4]),
        65210: ("3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0", [1,0,4,3,2]),
    }

    testResults = [runner(ti[0], ti[1]) == eo for (eo, ti) in testVectors.items()]
    if all(testResults):
        print("All tests passed!")
        inputFile = os.path.join(
            os.path.dirname(
                os.path.abspath(sys.argv[0])
            ), 'input'
        )
        if os.path.isfile(inputFile):
            with open(inputFile, 'r') as fh:
                program = fh.read()
            program = program.strip()
            print(main(program))
        else:
            print("Input file not found")
    else:
        print(testResults)

# vim: set filetype=python set foldmethod=marker
