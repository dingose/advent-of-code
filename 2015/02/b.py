#!/usr/bin/env python3

"""Advent of Code"""

import math
import os.path
import sys
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(sys.argv[0])))))
import toolbox as tb

from collections import defaultdict, deque
from itertools import cycle
from pprint import pprint

# tb.ints(str)
# tb.minmax(1, 2)
# tb.manhattan((x1,y1), (x2,y2))


def ribbonCalculator(data, debug=False):
    """Calculate the sum total ribbon needed
    for all boxes, each defined on a single line"""
    total = 0

    for box in data:
        sides = [int(side) for side in box.split('x')]
        volume = math.prod(sides)
        sides.pop(sides.index(max(sides)))
        total += sum([2 * side for side in sides]) + volume

    return total


def main(data):
    """Main algorithm"""
    return ribbonCalculator(data)


if __name__ == '__main__':
    testVectors = [
        {
            'expectedOutcome': 34,
            'testInput': {
                'data': ['2x3x4'],
                'debug': True,
            },
            'function': ribbonCalculator,
        },
        {
            'expectedOutcome': 14,
            'testInput': {
                'data': ['1x1x10'],
                'debug': True,
            },
            'function': ribbonCalculator,
        },
        {
            'expectedOutcome': 34 + 14,
            'testInput': {
                'data': ['2x3x4', '1x1x10'],
                'debug': True,
            },
            'function': ribbonCalculator,
        },
        # add more vectors here...
    ]

    testResults = [
        ({
            'index': idx,
            'state': (actualOutcome := tv['function'](**tv['testInput'])) == tv['expectedOutcome'],
            'expected': tv['expectedOutcome'],
            'received': actualOutcome,
        }, print("\aTest {} executed with result {}".format(idx, 'PASS' if actualOutcome == tv['expectedOutcome'] else 'FAIL')))[0]
        for idx, tv
        in enumerate(testVectors)
    ]
    if all([result['state'] for result in testResults]):
        print("All tests passed!")
        inputFile = os.path.join(
            os.path.dirname(
                os.path.abspath(sys.argv[0])
            ), 'input'
        )
        if os.path.isfile(inputFile):
            with open(inputFile, 'r') as fh:
                inputData = [line.strip() for line in fh.readlines()]
            print(main(inputData))
        else:
            print("Input file not found")
    else:
        pprint(testResults)

# vim: set filetype=python set foldmethod=marker
