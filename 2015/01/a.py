#!/usr/bin/env python3

"""Advent of Code"""

import os.path
import sys
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(sys.argv[0])))))
import toolbox as tb

# tb.ints(str)
# tb.minmax(1, 2)
# tb.manhattan((x1,y1), (x2,y2))


def main(data):
    """Main algorithm"""
    start = 0
    up = data.count('(')
    down = data.count(')')
    return (start + up) - down


if __name__ == '__main__':
    testVectors = {
        '(())': 0,
        '()()': 0,
        '(((': 3,
        '(()(()(': 3,
        '))(((((': 3,
        '())': -1,
        '))(': -1,
        ')))': -3,
        ')())())': -3,
    }

    testResults = [main(ti) == eo for (ti, eo) in testVectors.items()]
    if all(testResults):
        print("All tests passed!")
        inputFile = os.path.join(
            os.path.dirname(
                os.path.abspath(sys.argv[0])
            ), 'input'
        )
        if os.path.isfile(inputFile):
            with open(inputFile, 'r') as fh:
                print(main(fh.read()))
        else:
            print("Input file not found")
    else:
        print(testResults)

# vim: set filetype=python set foldmethod=marker
